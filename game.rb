# frozen_string_literal: true

# вызов rand(3) дает случайное число от 1 до 3, не включая 3 (см. документацию)
computer_choice = rand(3)

user_choice = rand(3)

# выводим вариант выбранный компом
puts 'Робот выбрал: ' + computer_choice.to_s

# выводим вариант выбранный человеком
puts 'Вы выбрали: ' + user_choice.to_s

if user_choice == computer_choice
  puts 'Ничья'
elsif user_choice == 0 && computer_choice == 1 # у вас 0-камень И у компьютера 1-ножницы
  puts 'Победили человек'
elsif user_choice == 1 && computer_choice == 2 # у вас ножницы И у компа бумага
  puts 'Победили человек'
elsif user_choice == 2 && computer_choice == 0 # бумага И камень
  puts 'Победили человек'
else # во ВСЕХ ОСТАЛЬНЫХ случаях победа за компом
  puts 'Победил робот'
end
